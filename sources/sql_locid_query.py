import boto3
import pandas as pd
import io
import re
import time
import yaml
import awswrangler as wr

with open('config.yaml') as file:
    io_cfg = yaml.load(file, Loader=yaml.FullLoader)

ACCESS_KEY = open(io_cfg['s3']['ID']).readlines()[0]
SECRET_KEY = open(io_cfg['s3']['PASSWORD']).readlines()[0]
session = boto3.Session(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    region_name='us-east-1'
)
s3 = session.resource('s3')
client = session.client('athena', region_name='us-east-1')

query = """ 
SELECT locationid AS Location,'Department' AS LocationAttribute, substr(parent,-3) AS Value
FROM locations
WHERE type = 'Location'

        """


def athena_query(query=query):
    df = wr.athena.read_sql_query(query, database='sftp-pll')
    df.reset_index(drop=True, inplace=True)
    return df

#
# def athena_query(client=client,query=query):
#
#     response = client.start_query_execution(
#         QueryString=query,
#         QueryExecutionContext={
#             'Database': 'sftp-pll'
#         },
#         ResultConfiguration={
#             'OutputLocation': "s3://sftp-adhoc-pll/Athena_locationid_log/"
#         }
#     )
#
#     query_execution_id = response["QueryExecutionId"]
#     return response
