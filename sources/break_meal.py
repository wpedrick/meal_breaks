import pandas as pd
import numpy as np
import yaml
from datetime import datetime,date
from sql_locid_query import athena_query
from s3_upload import s3_upfun,s3_downfun
import argparse

pd.set_option('display.max_columns',None)
pd.set_option('expand_frame_repr', False)

#Comment when debugging in pycharm


break_meal = pd.read_csv('break_meal.csv',dtype='str')
# locid_parent = pd.read_csv('sources/locid_parent.csv',dtype='str')
locid_parent= athena_query()

config_file = 'C:\\dev\\meal_breaks\\configs\\park_list.yml'
with open(config_file, 'r') as file:
    config = yaml.load(file, Loader=yaml.FullLoader)

#Overwrite config file with command line prompt
config['parks'] = args.parkid

now = datetime.now()
dt_string = now.strftime("%Y%m%d")


#Filter parks

for park in config['parks']:
    break_meal = break_meal[(break_meal['ParkCode'] == park)]

    # break_meal
    Standards_merge = break_meal.merge(locid_parent,left_on='DeptCode',right_on='value')

    Standards_meal = Standards_merge.rename(columns={"location":"Location","MealMinutes":"Value"})
    Standards_meal['LocationAttribute'] = 'Meal_Duration'
    Standards_meal = Standards_meal[['Location','LocationAttribute','Value']]
    Standards_meal.replace('0','30',inplace=True)
#Overrides
    # overrides = s3_downfun(path='s3://sftp-adhoc-pll/meal_breaks/Meal_Overrides.csv')
    # Standards_meal.merge(overrides)
    Standards_meal.to_csv(f'C:\\dev\\meal_breaks\\output\\Standards_mealminutes_{park}_{dt_string}.csv',index=False)

