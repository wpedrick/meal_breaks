import awswrangler as wr
import pandas as pd
import ntpath
from os import listdir
from os.path import isfile, join
import yaml
import logging
import boto3



with open('config.yaml') as file:
    io_cfg = yaml.load(file, Loader=yaml.FullLoader)

ACCESS_KEY = open(io_cfg['s3']['ID']).readlines()[0]
SECRET_KEY = open(io_cfg['s3']['PASSWORD']).readlines()[0]
session = boto3.Session(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    region_name='us-east-1'
)
s3 = session.resource('s3')
# s3 = session.client('s3')


def s3_upfun(df,path):
    wr.s3.to_csv(df=df,path=path,boto3_session=session,index=False)

def s3_upfun_parquet(df,path,dtype):
    wr.s3.to_parquet(df=df,path=path,boto3_session=session,index=True,dtype=dtype)

def s3_downfun(path):
    df = wr.s3.read_csv(path=path,boto3_session=session)
    return df

    # list = list(bucket.list("","/"))
    # filter = lambda x: True if file.startswith('Dem') else False
    # wr.s3.read_csv(file,path=path,dataset=True,partition_filter=filter,boto3_session=session)
